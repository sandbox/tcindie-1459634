<div class="subsection-item">
  <a name="<?php print(theme('subsection_jump_link_name', array('title' => $title))); ?>">
    <h3><?php print $title; ?></h3>
  </a>
  <?php if (!empty($body)): ?>
    <div class="subsection-body"><?php print $body; ?></div>
  <?php endif; ?>
  <?php if (!empty($refs)): ?>
    <div class="subsection-refs">
      <?php foreach ($refs as $ref) { print drupal_render($ref); } ?>
    </div>
  <?php endif; ?>
  <?php if (!empty($view)): ?>
    <?php print $view; ?>
  <?php endif; ?>
</div>